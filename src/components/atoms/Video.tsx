import React, { VideoHTMLAttributes, useEffect, useRef } from 'react';

type PropsType = VideoHTMLAttributes<HTMLVideoElement> & {
  srcObject?: MediaProvider | null;
};

export const Video = ({ srcObject, ...props }: PropsType) => {
  const videoRef = useRef<HTMLVideoElement>(null);

  useEffect(() => {
    if (!videoRef.current) return;
    videoRef.current.srcObject = srcObject ?? null;
  }, [srcObject]);

  return (
    <div className="Video">
      <video ref={videoRef} {...props} />
    </div>
  );
};
