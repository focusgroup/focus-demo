import React, { useEffect, useRef } from 'react';
import { Video } from '../components/atoms';

export interface HomePropsType {}

export const Home = (props: HomePropsType) => {
  const webcamStreamRef = useRef<MediaStream>();

  useEffect(() => {
    const constraints = { video: true, audio: false };

    const getWebcamStream = async () => {
      webcamStreamRef.current = await navigator.mediaDevices.getUserMedia(constraints);
    };

    getWebcamStream();
  });

  return (
    <div className="Home">
      <Video key="input-video" srcObject={webcamStreamRef.current} autoPlay />
      <Video key="encoded-video" srcObject={webcamStreamRef.current} autoPlay />
      {/*<Video key="decoded-video" srcObject={decode(encode(webcamStreamRef.current))} autoPlay />*/}
    </div>
  );
};
